#!/bin/bash

# Activate sudo early
sudo -v 

sudo dnf install -y ansible

ansible-playbook -K -i localhost, ./setup_for_ADB.yml

exec bash -i
